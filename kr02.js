/**
 * @name kr02
 *
 * @desc Proje kapsamı: KR 02

 * AEGON_RAPOR1.xlsx (Aegon-Ödeme Aksatan Poliçeler Exceli örneği)
 * AEGON_RAPOR2.xlsx (AEGON Üretim Exceli örneği)
 * AEGON_RAPOR3.xlsx (Aegon-Acente Komisyon Raporu örneği)

Adım 1-Adım 5: İptal
Adım 6: AEGON_RAPOR_3 Bu excelde aşağıdaki sütunlar silinir:
  BRANCH, BRANCH_CODE, APPLICATION_NUM, CUSTOMER_NAME, ADVISOR_TC _IDENTITY_NUM, ADVISOR_NAME, AGENCY_NUMBER, AGNCY_NAME, PERIOD_OF_PAYING
Adım 7: En başa “Danışman” adında yeni bir sütun eklenir.
Adım 8: AEGON Üretim Exceli okunur. Bir örneği AEGON_RAPOR2 olarak bu dosyaya ek olarak konulmuştur. Bu excelin ilk sütunu “Danışman Adı”, ikinci sütunu “Müşteri Adı”dır.
Adım 9: AEGON_RAPOR3’deki (Aegon-Acente Komisyon Raporu) CUSTOMER_NAME sütunundaki her isim, ile AEGON_RAPOR2’deki (Aegon Üretim Excel’i) ikinci sütun (Müşteri Adı) karşılaştırılır. Eşleşme olduğu zaman, AEGON_RAPOR2’deki ilk sütun (Danışman Adı), AEGON_RAPOR3’deki 8. Adımda oluşturulmuş “Danışman” sütununa yazılır.
  Özetle, acente komisyon raporundaki müşteriler excelindeki müşterilerin, hangi danışmana ait olduğu üretim raporunda aratılıp, eşleşme olunca, ödeme aksatan müşteri exceline yazılmaktadır.
  İsimler normal şartlar altında “kimlikte yazıldığı gibi” yazılmaktadır, fakat ı,i harfleri, büyük, küçük harf gibi konularda uyuşmamazlık olabilir. Alternatif senaryolar zaman içinde görüldükçe robota eklenecektir.
  Bu adım AEGON_RAPOR1 (Aegon-Ödeme Aksatan Poliçeler) raporundaki tüm müşteriler için eşleştirme yapılıncaya ve “Danışman” sütununa yazılıncaya kadar devam eder.
Adım 10: TODO: Bu adım neden yok ?
Adım 11: Toplu rapor, “TRANSACTION_DESCRIPTION” alanından aşağıdaki koşullara filtrelenip 2 ayrı rapor oluşturulur:
  a- “İptal”
  b- “3 .Taksit” veya “4 .Taksit”
Adım 12: İki toplu rapor da İstiklal Hanım’a mail atılır. (istiklal@istiklalsigortacilik.com.tr)
Adım 13: 2 excel her “Danışman” için ayrı ayrı parçalara ayrılır. Parçalanmış exceller her danışmana ayrı ayrı mail atılır.
*/

"use strict";

/***********
MODULES
************/
const XLSX=require('xlsx');
const fs = require('fs')
const nodemailer = require('nodemailer');
const moment = require('moment');
var pyshell = require('python-shell');

/***********
DEFINITIONS
************/

// excel related items
var EXCEL_PATH;
var EXCEL_RESULTS;
var EXCEL_AEGON_DELAYED
var EXCEL_AEGON_PRODUCTION;
var EXCEL_AEGON_AGENCY;
var sheetarray_aegon_delay;
var sheetarray_aegon_production;
var sheetarray_aegon_agency;
var sheetarray_result;
var sheetarray_iptals = [];
var sheetarray_taksits = [];
var consultant_excels = [];
var consultants_array  = [];
var yyy = 0;

// mail informations
const MAIL_FROM = 'ertanuysal@gmail.com'
const MAIL_PASSWORD = 'xxxxxxxxxx'
const MAIL_ISTIKLAL = 'ertanuysal@gmail.com'
const MAIL_DANISMAN_GLOBAL = 'ertanuysal@gmail.com'

// external python program name
var PYTHON_PROGRAM = "/projects/freelances/mert/kronnikaexcel/kronnikaExcel.py";

// debug level
var debug_level = 1;

// Sheet to array
var sheet2arr = function(sheet){
    var result = [];
    var row;
    var rowNum;
    var colNum;
    var range = XLSX.utils.decode_range(sheet['!ref']);
    for(rowNum = range.s.r; rowNum <= range.e.r; rowNum++){
       row = [];
        for(colNum=range.s.c; colNum<=range.e.c; colNum++){
           var nextCell = sheet[
              XLSX.utils.encode_cell({r: rowNum, c: colNum})
           ];
           if( typeof nextCell === 'undefined' ){
              row.push(void 0);
           } else row.push(nextCell.w);
        }
        result.push(row);
    }
    return result;
 };

// Array to sheet
var arr2sheet = function(sheetarray, excel_name)
{
  var ws_name = "Sheet";
  var wb = XLSX.utils.book_new(), ws = XLSX.utils.aoa_to_sheet(sheetarray);
  /* add worksheet to workbook */
  XLSX.utils.book_append_sheet(wb, ws, ws_name);
  /* write workbook */
  try {
    fs.unlinkSync(EXCEL_RESULTS + excel_name)
  }
  catch(error) {
  }

  fs.mkdirSync(EXCEL_RESULTS, { recursive: true });
  XLSX.writeFile(wb, EXCEL_RESULTS + excel_name);
}

// Convert turkish to english for step 10
String.prototype.turkishtoEnglish = function () {
  return this.replace(/Ğ/gim, "g")
    .replace(/Ü/gim, "u")
    .replace(/Ş/gim, "s")
    .replace(/I/gim, "i")
    .replace(/İ/gim, "i")
    .replace(/Ö/gim, "o")
    .replace(/Ç/gim, "c")
    .replace(/ğ/gim, "g")
    .replace(/ü/gim, "u")
    .replace(/ş/gim, "s")
    .replace(/ı/gim, "i")
    .replace(/ö/gim, "o")
    .replace(/ç/gim, "c");
};

// send mail with attached file
async function mail_sender(mail_to, mail_subject, attached_file_1, attached_file_2)
{
   var mail = nodemailer.createTransport({
   service: 'gmail',
       auth: {
       user: MAIL_FROM,
       pass: MAIL_PASSWORD
   }
   });

   var mailOptions = {
       from: MAIL_FROM,
       to: mail_to,
       subject: mail_subject
   }

  // attach the second one if it is not empty
  if (attached_file_2){
    mailOptions.attachments = [{path: EXCEL_RESULTS + attached_file_1, filename: attached_file_1},
                              {path: EXCEL_RESULTS + attached_file_2, filename: attached_file_2}];
  }
  else {
    mailOptions.attachments = [{path: EXCEL_RESULTS + attached_file_1, filename: attached_file_1}];
  }

  mail.sendMail(mailOptions, function(error, info){
    if (error) {
      logger("Email could not be sent", "error");
    } else {
      logger('Email sent: ' + info.response, "info");
    }
  });
}

// Initialize all needed globals
async function init_all()
{
  logger("read all excel files to the seperated arrays at a time", "info");

  var args = process.argv.slice(2);
  if(args[1] == "linux") {
    EXCEL_PATH = process.cwd()+"/excels/"
    EXCEL_RESULTS = EXCEL_PATH + "results/"
    EXCEL_AEGON_DELAYED = EXCEL_PATH + 'AEGON_RAPOR1.xlsx';
    EXCEL_AEGON_PRODUCTION = EXCEL_PATH + 'AEGON_RAPOR2.xlsx';
    EXCEL_AEGON_AGENCY = EXCEL_PATH + 'AEGON_RAPOR3.xlsx';
  }
  else
  {
    EXCEL_PATH = process.cwd()+"\\excels\\"
    EXCEL_RESULTS = EXCEL_PATH + "results\\"
    EXCEL_AEGON_DELAYED = EXCEL_PATH + 'AEGON_RAPOR1.xlsx';
    EXCEL_AEGON_PRODUCTION = EXCEL_PATH + 'AEGON_RAPOR2.xlsx';
    EXCEL_AEGON_AGENCY = EXCEL_PATH + 'AEGON_RAPOR3.xlsx';
  }

  const workbook_aegon_delay=XLSX.readFile(EXCEL_AEGON_DELAYED);
  const workbook_aegon_production=XLSX.readFile(EXCEL_AEGON_PRODUCTION);
  const workbook_aegon_agency=XLSX.readFile(EXCEL_AEGON_AGENCY);

  sheetarray_aegon_delay = sheet2arr(workbook_aegon_delay.Sheets[workbook_aegon_delay.SheetNames[0]]);
  sheetarray_aegon_production = sheet2arr(workbook_aegon_production.Sheets[workbook_aegon_production.SheetNames[0]]);
  sheetarray_aegon_agency = sheet2arr(workbook_aegon_agency.Sheets[workbook_aegon_agency.SheetNames[0]]);
}

// debug enable/disable from command line
async function init_debug()
{
  var args = process.argv.slice(2);
  switch(args[0]) {
    case "debug"  : debug_level = 3; break;
    case "info"   : debug_level = 2; break;
    case "error"  : debug_level = 1; break;
    default       : debug_level = 1;
  }

  logger("Debug level was set to: " + debug_level, "error" );
}

// Consol debug log
function logger(extra_log, loglevel)
{
  var tmp_level;

  var color_red    = '\x1b[31m%s\x1b[0m';
  var color_cyan   = '\x1b[36m%s\x1b[0m';
  var color_yellow = '\x1b[33m%s\x1b[0m';
  var color_log;

  switch(loglevel) {
    case "debug"  : tmp_level = 3; color_log = color_yellow; break;
    case "info"   : tmp_level = 2; color_log = color_cyan; break;
    case "error"  : tmp_level = 1; color_log = color_red; break;
    default       : tmp_level = 1;
  }

  if(debug_level >= tmp_level) {
    console.log(color_log, extra_log);
  }
}

/*
Adım 6: AEGON_RAPOR_3 Bu excelde aşağıdaki sütunlar silinir:
  BRANCH, BRANCH_CODE, APPLICATION_NUM, CUSTOMER_NAME, ADVISOR_TC _IDENTITY_NUM, ADVISOR_NAME, AGENCY_NUMBER, AGNCY_NAME, PERIOD_OF_PAYING
*/
async function step_6()
{
  logger("Remove no needed columns", "info");
  sheetarray_result = sheetarray_aegon_agency.map(function(arr) {
    return arr.slice();
  });

  for (let i = 0; i < sheetarray_result.length; i++) {
    sheetarray_result[i].splice(1, 8);
    sheetarray_result[i].splice(4, 1);
  }

  // save the result as an excel if debug is enabled
  if(debug_level == 3) {
    arr2sheet(sheetarray_result, "step_6.xlsx");
    logger("step_6.xlsx has been saved", "debug");
  }
}

// Adım 7: En başa “Danışman” adında yeni bir sütun eklenir.
async function step_7()
{
  logger("Add a new column named Danışman ", "info");

  sheetarray_result[0].unshift("Danışman");
  for (let i = 1; i < sheetarray_result.length; i++) {
    sheetarray_result[i].unshift(void 0);
  }

  // save the result as an excel if debug is enabled
  if(debug_level == 3) {
    arr2sheet(sheetarray_result, "step_7.xlsx");
    logger("step_7.xlsx has been saved", "debug");
  }
}

/*
Adım 9: AEGON_RAPOR3’deki (Aegon-Acente Komisyon Raporu) CUSTOMER_NAME sütunundaki her isim, ile AEGON_RAPOR2’deki (Aegon Üretim Excel’i) ikinci sütun (Müşteri Adı) karşılaştırılır. Eşleşme olduğu zaman, AEGON_RAPOR2’deki ilk sütun (Danışman Adı), AEGON_RAPOR3’deki 8. Adımda oluşturulmuş “Danışman” sütununa yazılır.
  Özetle, acente komisyon raporundaki müşteriler excelindeki müşterilerin, hangi danışmana ait olduğu üretim raporunda aratılıp, eşleşme olunca, ödeme aksatan müşteri exceline yazılmaktadır.
  İsimler normal şartlar altında “kimlikte yazıldığı gibi” yazılmaktadır, fakat ı,i harfleri, büyük, küçük harf gibi konularda uyuşmamazlık olabilir. Alternatif senaryolar zaman içinde görüldükçe robota eklenecektir.
  Bu adım AEGON_RAPOR1 (Aegon-Ödeme Aksatan Poliçeler) raporundaki tüm müşteriler için eşleştirme yapılıncaya ve “Danışman” sütununa yazılıncaya kadar devam eder.
*/
async function step_9()
{
  var turkish_agency, turkish_production;

  logger("Fil column->Danışman ", "info");
  var found_consultant;
  for (let i = 1; i < sheetarray_aegon_agency.length; i++) {
    found_consultant = 0;
    for (let j = 0; j < sheetarray_aegon_production.length; j++) {
      try {
        turkish_agency = sheetarray_aegon_agency[i][4].turkishtoEnglish();
        turkish_production = sheetarray_aegon_production[j][1].turkishtoEnglish();
        logger(turkish_agency.toLowerCase() + "==" +  turkish_production.toLowerCase(), "debug");
        if(turkish_agency.toLowerCase() == turkish_production.toLowerCase()) {
          logger("i: " + i + " matched: " + sheetarray_aegon_agency[i][4], "debug");
          sheetarray_result[i][0] = sheetarray_aegon_production[j][0];
          found_consultant = 1;
          continue;
        }
      }
      catch(error) {
        continue
      }
    }
    if(found_consultant == 0) {
      logger("not found: " + sheetarray_aegon_agency[i][4], "error");
      sheetarray_result[i][0] = "danışman eşleşemedi";
    }
  }

  arr2sheet(sheetarray_result, "result.xlsx");
  logger("step_9: result.xlsx has been saved", "info");
}

/*
Adım 11: Toplu rapor, “TRANSACTION_DESCRIPTION” alanından aşağıdaki koşullara filtrelenip 2 ayrı rapor oluşturulur:
  a- “İptal”
  b- “3 .Taksit” veya “4 .Taksit”
Adım 12: İki toplu rapor da İstiklal Hanım’a mail atılır. (istiklal@istiklalsigortacilik.com.tr)
Adım 13: 2 excel her “Danışman” için ayrı ayrı parçalara ayrılır. Parçalanmış exceller her danışmana ayrı ayrı mail atılır.
*/
async function step_11()
{
  var transaction_description;

  logger("step_11: split two excel files (İptal and 3/4 taksit)", "info");

  /* Prepare a tmp array which will be filled by the step 9 result */
  sheetarray_iptals[0] = sheetarray_result[0];
  sheetarray_taksits[0] = sheetarray_result[0];
  var k = 1;
  var m = 1;
  for (let i = 1; i < sheetarray_result.length; i++) {
    transaction_description = sheetarray_result[i][6];
    logger("No: " + i + " transaction_description : " + transaction_description, "debug");
    switch(transaction_description) {
      case "İptal":
        logger("Found İptal. No: " + i, "debug");
        sheetarray_iptals[k] = sheetarray_result[i];
        k++;
      break;
      case "3 .Taksit":
      case "4 .Taksit":
        logger("Found 3/4 Taksit. No: " + i, "debug");
        sheetarray_taksits[m] = sheetarray_result[i];
        m++;
        break;
      default: logger("No found: " + transaction_description, "debug");
    }
  }

  arr2sheet(sheetarray_iptals, "result_cancel.xlsx");
  logger("step_11: result_cancel.xlsx has been saved", "info");

  arr2sheet(sheetarray_taksits, "result_taksit.xlsx");
  logger("step_11: result_taksit.xlsx has been saved", "info");
}

// Adım 13: 2 excel her “Danışman” için ayrı ayrı parçalara ayrılır. Parçalanmış exceller her danışmana ayrı ayrı mail atılır.
async function step_13_sub(sheetarray, excelname)
{
  logger("Split the " + excelname + " for each consultant and mail the results ", "info");

  /* Fill a new array with the consultants (remove empty lines, duplicates, just the consultants */
  var searched_consultant = [];
  var k = 0;
  var is_consultant_taken;
  for (let i = 1; i < sheetarray.length; i++) {
    if(sheetarray[i][0]){
      is_consultant_taken = 0;
      for (let j = 0; j < searched_consultant.length; j++) {
        if(sheetarray[i][0] == searched_consultant[j]) {
          is_consultant_taken = 1;
          break;
        }
      }
      if(!is_consultant_taken && sheetarray[i][0] != "danışman eşleşemedi") {
        searched_consultant[k] = sheetarray[i][0];
        k++;
      }
    }
  }
  logger("All found consultants", "debug");
  logger(searched_consultant, "debug");

  /* Search for each consultant */
  var tmp_consultant_array = [];
  for (let i = 0; i < searched_consultant.length; i++) {
    tmp_consultant_array = []
    tmp_consultant_array[0] = sheetarray[0];
    k = 1;
    for (let j = 1; j < sheetarray.length; j++) {
      if(searched_consultant[i] == sheetarray[j][0]) {
        tmp_consultant_array[k] = sheetarray[j];
        k++;
      }
    }
    var consultant_excel_name = "result_" + excelname + "_" + searched_consultant[i].split(" ").join("_") + ".xlsx";
    logger(" Created new excel (Consultant: " + searched_consultant[i] + " File: " + consultant_excel_name + ")", "debug");
    consultant_excels[yyy] = consultant_excel_name;
    consultants_array[yyy] = searched_consultant[i];
    yyy++;
    arr2sheet(tmp_consultant_array, consultant_excel_name);
  }
}

// Adım 13: 2 excel her “Danışman” için ayrı ayrı parçalara ayrılır. Parçalanmış exceller her danışmana ayrı ayrı mail atılır.
async function step_13()
{
  step_13_sub(sheetarray_iptals, "iptals");
  step_13_sub(sheetarray_taksits, "taksits");
}

// convert non formatted excel to kronnika format via a pyhton app
async function convert_kronnika_and_mail()
{
  var options = {
    args: [EXCEL_RESULTS]
  };
  pyshell.PythonShell.run(PYTHON_PROGRAM, options, function  (err, results)  {
    if  (err)  throw err;
    logger(PYTHON_PROGRAM + ' finished with ' + results, "info");
    send_mails();
  });
}

// Adım 11: aegon: Eşleştirme yapılmış toplu excel (Aegon-Ödeme Aksatan Poliçeler), İstiklal Hanım’a mail atılır. (istiklal@istiklalsigortacilik.com.tr)
// Adım 12: aegon: Parçalanmış exceller her danışmana ayrı ayrı mail atılır.
// Adım 32: allianz: Eşleştirme yapılmış toplu excel (Allianz-Tahsil Edilmemiş Katkı Payı Vadeleri), İstiklal Hanım’a mail atılır. (istiklal@istiklalsigortacilik.com.tr)
// Adım 33: allianz: Parçalanmış exceller her danışmana ayrı ayrı mail atılır.
async function send_mails()
{
  logger("Send the result_cancel.xls and result_taksit.xls to Ms. İstiklal ", "info");
  mail_sender(MAIL_ISTIKLAL, 'Results: Ms. İstiklal', 'result_cancel.xlsx', 'result_taksit.xlsx');

  for (let i = 1; i < consultants_array.length; i++) {
    mail_sender(MAIL_DANISMAN_GLOBAL, 'Results: ' + consultants_array[i].split(" "), consultant_excels[i], '');
  }
}

init_all();
init_debug();
step_6();
step_7();
// Skip Adım 8 (init_all içerisinde yapıldı)
step_9();
// Skip Adım 10: TODO: Bu adım neden yok ?
step_11();
step_13();
convert_kronnika_and_mail();